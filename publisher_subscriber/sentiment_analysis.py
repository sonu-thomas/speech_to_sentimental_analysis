#!/usr/bin/env python
import rospy
from std_msgs.msg import String

# Function for display the subscribed text from node speech_to_text 
def callback(data):
    rospy.loginfo(rospy.get_caller_id() + "I heard %s",data.data)
    # Program for sentiment analysis
    p=["good", "happyman", "nice", "super", "awsome", "wonderful"]
    n=["bad", "sad", "mad", "expensive"]
    ne=["ok", "nothanks"]
    for i in p:
        if i==data.data:
            s= "the word is a positive one: "

	    # Declare the publisher node listen and publishes the 
            # sentiment corresponding to the input speech 
            pub = rospy.Publisher('voicerecognition',String,queue_size=10)
            rospy.init_node('listener',anonymous=True)
            rate = rospy.Rate(10) # 10hz
            while not rospy.is_shutdown():
                y=s
                rospy.loginfo(y)
                pub.publish(y)
                rate.sleep()
		
    for i in n:
        if i==data.data:
	    s="the word is a negetive one:"
	    pub = rospy.Publisher('voicerecognition',String,queue_size=10)
	    rospy.init_node('listener',anonymous=True)
            rate = rospy.Rate(10) # 10hz
            while not rospy.is_shutdown():
                y=s
                rospy.loginfo(y)
                pub.publish(y)
                rate.sleep()
				  
    for i in ne:
        if i==data.data:
             s="the word is a neutral one:" 
	     pub = rospy.Publisher('voicerecognition',String,queue_size=10)
             rospy.init_node('listener',anonymous=True)
             rate = rospy.Rate(10) # 10hz
             while not rospy.is_shutdown():
                 y=s
                 rospy.loginfo(y)
                 pub.publish(y)
                 rate.sleep()
   
# Function for subscribing data published from node speech_to_text
def listener():
    
       # In ROS, nodes are uniquely named. If two nodes with the same
       # node are launched, the previous one is kicked off. The
       # anonymous=True flag means that rospy will choose a unique
       # name for our 'listener' node so that multiple listeners can
       # run simultaneously.
    rospy.init_node('listener',anonymous=True)
   
    rospy.Subscriber("voicerecognition",String,callback)
   
    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()
# Main function starts
if __name__ == '__main__':
    
    
    listener()
