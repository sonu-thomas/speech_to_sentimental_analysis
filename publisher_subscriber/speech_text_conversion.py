#!/usr/bin/env python
# license removed for brevity

# Python 2.x program to covert an audio file
import rospy
from std_msgs.msg import String
import speech_recognition as sr
r=sr.Recognizer()

# Function to convert speech to text 
def speech_to_text_conversion():

    # Use the default microphone as the audio source
    with sr.Microphone() as source:
        print("say something")
	# Listen for the first phrase and extract it into audio data
    	audio =r.listen(source) 

    # Defines the talker's interface to the rest of ROS
    pub = rospy.Publisher('voicerecognition',String,queue_size=10)
    rospy.init_node('talker',anonymous=True)
    rate = rospy.Rate(10) # 10hz
 
    # To publish the converted text  
    while not rospy.is_shutdown():
        y=r.recognize_google(audio)
	rospy.loginfo(y)
        pub.publish(y)
        rate.sleep()

# Starts main function  
if __name__ == '__main__':
    try:
        speech_to_text_conversion()
    except rospy.ROSInterruptException:
        pass

