#!/usr/bin/env python

from speech_services.srv import *
import rospy
#handle function definition
def handle_analysis(req):
#creating neutral, positive and negative word list
    neutral=['hello','hai','day','time']
    positive=['smile','happy','beautiful','good']
    negative=['no','bad','cry','sorrow']
#getting data from client
    z=req.a
    print "converted text is ",req.a
    print "Returning"
#performing sentimental analysis of converted text
    for i in neutral:
      if i==z:
        m='The text is neutral'
    
    for i in positive:
      if i==z:
        m='The text is positive'
    for i in negative:
      if i==z:
        m='The text is negative'
#returning the result back to client
    return SpeechResponse(m)

def speech_server():
#creating server node
    rospy.init_node('speech_server')
    s = rospy.Service('analysis', Speech, handle_analysis)
    print "Analysis is ready"
    rospy.spin()
#start of main function
if __name__ == "__main__":
    speech_server()
