Project Description

	In this speech to sentimental analysis , we give speech as input and it get converted to sentiment. Then the output is given according to whether the speech is positive, negetive or neutral.
Eg: good - positive
       bad - negetive
       ok – neutral
Here the program is done using three different mode of communication between the nodes in ROS.

Installing ROS Kinetic on python 2.7 :
ROS Kinetic Kame is primarily targeted at the Ubuntu 16.04 (Xenial) release.
	
		http://wiki.ros.org/kinetic/Installation/Ubuntu

Installing pip/setuptools with Linux Package Managers:
          If you’re using a Python that was downloaded from  https://www.python.org/
, then this section does not apply. See the Requirements for Installing Packages section instead.

sudo apt-get install pip 

Verify the Pip Installation:

pip -V   

pip install SpeechRecognition :
•
  pip install SpeechRecognition

To quickly try it out, run python -m speech_recognition after installing.

Requirements
To use all of the functionality of the library, you should have:
• Python 2.6, 2.7, or 3.3+ (required) 
• PyAudio 0.2.11+ (required only if you need to use microphone input, 
Microphone) 
• Google API Client Library for Python (required only if you need to use 
the Google Cloud Speech API, recognizer_instance.recognize_google_cloud) 
On Debian-derived Linux distributions (like Ubuntu and Mint), install PyAudio 
using APT:  execute sudo apt-get install python-pyaudio python3-pyaudio in a 
terminal.
  


Examining the Simple service client :
Here we are deals with how to write a service client in  python. 
Creating a catkin Package:
cd ~/catkin_ws/src

catkin_create_pkg beginner_tutorials std_msgs rospy roscpp

Building a catkin workspace and sourcing the setup file:
$ cd ~/catkin_ws
$ catkin_make

Writing a Simple Service and Client  (Python):
http://wiki.ros.org/ROS/Tutorials/WritingServiceClient%28python%29

follow this link to create the service and client node.
